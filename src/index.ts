import express, { Express } from 'express'
import bodyParser from 'body-parser'

// Controllers
import * as rootController from './controller/rootController'
import * as moviesController from './controller/moviesController'

const server: Express = express()

server.use(bodyParser.json())
server.use(bodyParser.urlencoded({
    extended: true
}))

// Services
server.get('/', rootController.sendDeafultMessage)
server.get('/movies', moviesController.sendMoviesInfo)

server.listen(3000, () => {
    console.log('Server listening at port 3000')
})